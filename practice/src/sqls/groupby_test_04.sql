/*
 * 请告诉我总金额大于 60000 的每一个订单（`order`）编号及其总金额。查询结果应当包含如下信息：
 * 
 * +──────────────+─────────────+
 * | orderNumber  | totalPrice  |
 * +──────────────+─────────────+
 *
 * 其结果应当以 `orderNumber` 排序。
 */
SELECT o.orderNumber, SUM(d.quantityOrdered * d.priceEach) AS totalPrice
FROM orderdetails AS d
         RIGHT JOIN orders AS o
                    ON d.orderNumber = o.orderNumber
GROUP BY o.orderNumber
HAVING totalPrice > 60000
ORDER BY o.orderNumber;