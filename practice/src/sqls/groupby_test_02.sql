/*
 * 请告诉我所有的订单（`order`）中每一种 `status` 的订单的总金额到底是多少。注意是总金额哦。输出
 * 应当包含如下的信息：
 *
 * +─────────+─────────────+
 * | status  | totalPrice  |
 * +─────────+─────────────+
 *
 * 输出应当根据 `status` 进行排序。
 */
SELECT o.status AS status, SUM(orderTotalPrice.summationPrice) AS totalPrice
FROM orders AS o
         LEFT JOIN (SELECT orderNumber, SUM(totalPrice) AS summationPrice
                    FROM (SELECT d.orderNumber AS orderNumber, d.quantityOrdered * d.priceEach AS totalPrice
                          FROM orderdetails AS d) AS stupid
                    GROUP BY orderNumber)
    AS orderTotalPrice
                   ON orderTotalPrice.ordernuMBER = o.orderNumber
GROUP BY o.status;