/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */
SELECT MIN(counts)           AS minOrderItemCount,
       MAX(counts)           AS maxOrderItemCount,
       ROUND(AVG(counts), 0) AS avgOrderItemCount
FROM (SELECT COUNT(d.orderNumber) AS counts
      FROM orders AS o
               LEFT JOIN orderdetails AS d
                         ON o.orderNumber = d.orderNumber
      GROUP BY o.orderNumber) AS allorders;